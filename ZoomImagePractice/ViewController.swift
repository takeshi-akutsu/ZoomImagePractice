//
//  ViewController.swift
//  ZoomImagePractice
//
//  Created by Takeshi Akutsu on 2018/08/31.
//  Copyright © 2018年 Takeshi Akutsu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var profileImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage)))
    }

    @objc func tapImage() {
        zoomImage(imageView: profileImageView)
    }
}




class TapGestureRecognizer: UITapGestureRecognizer {
    var objectDic: [String: Any] = [:]
}
extension UIViewController {
    
    func zoomImage(imageView targetImageView: UIImageView) {
        // 背景の黒いビューを初期値alpha=0で定義
        let backgroundView = UIView(frame: self.view.bounds)
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0
        self.view.addSubview(backgroundView)
        
        targetImageView.alpha = 0
        
        // 引数でもらったimageViewを元に遷移前のframe情報を取得しておく
        guard let initialFrame = targetImageView.superview?.convert(targetImageView.frame, to: nil) else { return }
        let zoomImageView = UIImageView(frame: initialFrame)
        zoomImageView.image = targetImageView.image
        zoomImageView.isUserInteractionEnabled = true
        zoomImageView.contentMode = targetImageView.contentMode
        zoomImageView.clipsToBounds = targetImageView.clipsToBounds
        self.view.addSubview(zoomImageView)
        
        UIView.animate(withDuration: 0.60, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            // 背景を黒くフェードイン
            backgroundView.alpha = 1
            
            // imageViewをズームしながらセンタリング
            let afterWidth = self.view.frame.width
            let afterHeight = (afterWidth / initialFrame.width) * initialFrame.height
            zoomImageView.frame.size = CGSize(width: afterWidth, height: afterHeight)
            zoomImageView.center = self.view.center
            
        }) { completed in
            // zoom outするためのgestureを登録
            let gesture = TapGestureRecognizer(target: self, action: #selector(self.zoomOutImage(sender:)))
            gesture.objectDic["targetImageView"] = targetImageView
            gesture.objectDic["backgroundView"] = backgroundView
            gesture.objectDic["zoomImageView"] = zoomImageView
            gesture.objectDic["initialFrame"] = initialFrame
            zoomImageView.addGestureRecognizer(gesture)
        }
    }
    
    @objc private func zoomOutImage(sender: TapGestureRecognizer) {
        guard let targetImageView = sender.objectDic["targetImageView"] as? UIImageView else { return }
        guard let backgroundView = sender.objectDic["backgroundView"] as? UIView else { return }
        guard let zoomImageView = sender.objectDic["zoomImageView"] as? UIImageView else { return }
        guard let initialFrame = sender.objectDic["initialFrame"] as? CGRect else { return }
        UIView.animate(withDuration: 0.60, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            backgroundView.alpha = 0
            zoomImageView.frame = initialFrame
        }) { completed in
            backgroundView.removeFromSuperview()
            zoomImageView.removeFromSuperview()
            targetImageView.alpha = 1
            
        }
    }
}
